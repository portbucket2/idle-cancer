using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FlyingText : MonoBehaviour
{
    [SerializeField] TMP_Text textComponent;
    [SerializeField] float flyDuration;
    [SerializeField] float displacement;
    [SerializeField] Color positiveColor;
    [SerializeField] Color negativeColor;
    int direction;

    public void Init(string text, int direction = 1, float displacement = -1f, float fontSize = -1f, float duration = -1f)
    {
        textComponent.text = direction > 0 ? $"+{text}" :  $"-{text}";
        textComponent.color = direction > 0 ? positiveColor : negativeColor;

        if (fontSize > 0f)
        {
            textComponent.fontSize = fontSize;
        }

        if (displacement > 0f)
        {
            this.displacement = displacement; 
        }

        if (duration > 0f)
        {
            this.flyDuration = duration;
        }

        this.direction = direction;
    }

    private IEnumerator Start()
    {
        float startTime = Time.time;
        float progress = 0f;
        Vector3 startPos = transform.localPosition;
        Vector3 targetPos = transform.localPosition + Vector3.up * direction * displacement;

        while (progress < 1f)
        {
            progress = Mathf.Clamp01((Time.time - startTime) / flyDuration);
            transform.localPosition = Vector3.Lerp(startPos, targetPos, progress);
            textComponent.alpha = (1f - progress);
            yield return null;
        }

        Destroy(gameObject);
    }
}
