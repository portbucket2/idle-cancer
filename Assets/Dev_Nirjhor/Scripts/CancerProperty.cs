﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class CancerProperty
{
    public string name;
    string levelPrefKey { get { return $"{name}_level"; } }

    [Header("Params:")]
    public int baseCost;
    [Range(1.001f, 1.15f)] public float costMultiplier;
    //public int basePower;
    //public float powerMultiplier;

    [Header("UI:")]
    public Image progressBar;
    public Button button;
    public Image costBG;
    public TMP_Text cost_Text;
    //public TMP_Text power_Text;
    public TMP_Text mutationProgress_Text;

    //Current params:
    public int level_ThisStage { get; private set; }
    public int level_Cumulative { get; private set; }
    float cost;
    //float power;
    public bool propertyCompleteThisStage { get; private set; }

    //Actions:
    public void Init()
    {
        level_ThisStage = PlayerPrefs.GetInt(levelPrefKey, 0);
        progressBar.fillAmount = (float)(level_ThisStage % (CancerController.Instance.maxMutationLevel + 1)) /
            (float)CancerController.Instance.maxMutationLevel;
        level_Cumulative = GetCumulativeMutationLevel();

        //power = basePower * Mathf.Pow(powerMultiplier, level_Cumulative);
        cost = baseCost * Mathf.Pow(costMultiplier, level_Cumulative);

        cost_Text.text = $"{DataFormater.FormattedRNACount((int)cost)}";
        //power_Text.text = $"{DataFormater.FormattedRNACount((int)power)}";

        button.onClick.AddListener(() => Upgrade());

        if (level_ThisStage >= CancerController.Instance.maxMutationLevel)
        {
            button.interactable = false;
            propertyCompleteThisStage = true;
        }

        mutationProgress_Text.text = $"{(progressBar.fillAmount * 100f).ToString("F2")}%";
    }

    int GetCumulativeMutationLevel()
    {
        int cumulativeLevel = 0;
        for (int i = 1; i < CancerController.Instance.currentCancerStage; i++)
        {
            cumulativeLevel += CancerController.Instance.maxMutationLevels[i - 1];
        }
        cumulativeLevel += level_ThisStage;
        return cumulativeLevel;
    }

    public void Upgrade()
    {
        if (RNAController.Instance.currentRNA >= cost)
        {
            RNAController.Instance.SpendRNA((int)cost, costBG.transform);

            level_ThisStage++;
            level_Cumulative++;
            progressBar.fillAmount = (float)(level_ThisStage % (CancerController.Instance.maxMutationLevel + 1)) /
            (float)CancerController.Instance.maxMutationLevel;

            //power = basePower * Mathf.Pow(powerMultiplier, level_Cumulative);
            cost = baseCost * Mathf.Pow(costMultiplier, level_Cumulative);

            cost_Text.text = $"{DataFormater.FormattedRNACount((int)cost)}";
            //power_Text.text = $"{DataFormater.FormattedRNACount((int)power)}";

            if (level_ThisStage >= CancerController.Instance.maxMutationLevel)
            {
                button.interactable = false;
                propertyCompleteThisStage = true;
                RNAController.Instance.UpgradeRNARate_Passive();
            }

            mutationProgress_Text.text = $"{(progressBar.fillAmount * 100f).ToString("F2")}%";

            CancerController.Instance.UpdateCancerProgress();
        }
        else
        {
            InsufficientFundsError();
        }
    }

    public void ResetPrpertyOnStageChange()
    {
        propertyCompleteThisStage = false;
        level_ThisStage = 0;
    }

    public void ResetPropertyVisualOnStageContinue()
    {
        button.interactable = true;
        progressBar.fillAmount = 0f;
        mutationProgress_Text.text = $"{(progressBar.fillAmount * 100f).ToString("F2")}%";
    }

    void InsufficientFundsError()
    {
        costBG.color = Color.red;
        LeanTween.delayedCall(0.1f, () => costBG.color = Color.white);
    }

    public void SavePropertyLevel()
    {
        PlayerPrefs.SetInt(levelPrefKey, level_ThisStage);
    }
}