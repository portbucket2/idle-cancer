using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RNAController : MonoBehaviour
{
    public static RNAController Instance { get; private set; }

    #region Serialized Fields
    [SerializeField] GameSettings gameSettings;

    [Header("UI:")]
    [SerializeField] TMP_Text rnaCount_Text;
    [SerializeField] TMP_Text rnaRatePassive_Text;

    [SerializeField] Button rnaActiveIncome_Button;
    [SerializeField] TMP_Text rnaRateActive_Text;

    [SerializeField] Button rnaRateUpgrade_Button;
    [SerializeField] GameObject rnaRateUpgrade_Panel;
    [SerializeField] Button quitUpgrade_Button;

    //RNA Upgrade:
    [SerializeField] Button rnaRateActiveUpgrade_Button;
    [SerializeField] Button rnaRatePassiveUpgrade_Button;
    [SerializeField] Button rnaRateOfflineUpgrade_Button;

    [SerializeField] TMP_Text rnaRateActiveUpgradeCost_Text;
    [SerializeField] TMP_Text rnaRatePassiveUpgradeCost_Text;
    [SerializeField] TMP_Text rnaRateOfflineUpgradeCost_Text;

    [SerializeField] Image rnaRateActiveUpgradeCostBG_Img;
    [SerializeField] Image rnaRatePassiveUpgradeCostBG_Img;
    [SerializeField] Image rnaRateOfflineUpgradeCostBG_Img;

    //RNA Upgrade Panel:
    [SerializeField] TMP_Text rnaRateNextUpgrade_Active_Text;
    [SerializeField] TMP_Text rnaRateNextUpgrade_Passive_Text;
    [SerializeField] TMP_Text rnaRateNextUpgrade_Offline_Text;

    //TextFly:
    [SerializeField] FlyingText flyingText_Prefab;

    [SerializeField] Image dnaFly_Prefab;

    //Welcome Back:
    [SerializeField] GameObject welcomeBack_Panel;
    [SerializeField] TMP_Text offlineIncome_Text;
    [SerializeField] Button collect_Button;
    #endregion


    public ulong currentRNA { get; private set; } = 0;
    public ulong spentRNA { get; private set; } = 0;
    float currentRNARate_Active;
    float currentRNARate_Passive;
    float currentRNARate_Offline;

    int currentRNARate_Active_Level;
    int currentRNARate_Passive_Level;
    int currentRNARate_Offline_Level;

    float currentRNARateUpgrade_Active_Cost;
    float currentRNARateUpgrade_Passive_Cost;
    float currentRNARateUpgrade_Offline_Cost;


    bool passiveIncomeActive;
    float passiveIncomeCache;
    float activeIncomeCache;

    System.DateTime gameStartTime;

    SceneController sceneController;
    AnalyticsController analytics;
    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        sceneController = GameController.GetController().GetSceneController();
        InitData();

        ConfigureUIButtons();
        analytics = AnalyticsController.GetController();
    }

    void ConfigureUIButtons()
    {
        rnaActiveIncome_Button.onClick.AddListener(() => IncrementRNA_Active());

        rnaRateUpgrade_Button.onClick.AddListener(() => 
        {
            analytics.EventButtonPress("Upgrade", 1f);

            rnaRateUpgrade_Button.enabled = false;
            quitUpgrade_Button.enabled = false;

            LeanTween.scale(rnaRateUpgrade_Panel, Vector3.one, 0.5f).setEase(LeanTweenType.easeOutBack).setOnComplete(() => quitUpgrade_Button.enabled = true);
            LeanTween.scale(rnaRateUpgrade_Button.gameObject, Vector3.zero, 0.5f).setEase(LeanTweenType.easeInBack);
        });

        quitUpgrade_Button.onClick.AddListener(() =>
        {
            quitUpgrade_Button.enabled = false;
            rnaRateUpgrade_Button.enabled = false;

            LeanTween.scale(rnaRateUpgrade_Panel, Vector3.zero, 0.5f).setEase(LeanTweenType.easeInBack);
            LeanTween.scale(rnaRateUpgrade_Button.gameObject, Vector3.one, 0.5f).setEase(LeanTweenType.easeOutBack).setOnComplete(() => rnaRateUpgrade_Button.enabled = true);
        });

        rnaRateActiveUpgrade_Button.onClick.AddListener(() => UpgradeRNARate_Active());
        rnaRatePassiveUpgrade_Button.onClick.AddListener(() => UpgradeRNARate_Passive());
        rnaRateOfflineUpgrade_Button.onClick.AddListener(() => UpgradeRNARate_Offline());

        if (currentRNARate_Active_Level > gameSettings.rnaRateUpgradeResolution_Active)
        {
            rnaRateActiveUpgrade_Button.interactable = false;
        }
    }

    public void OnGameplayStart()
    {
        passiveIncomeActive = true;
        StartCoroutine(PassiveIncome());
    }

    IEnumerator PassiveIncome()
    {
        WaitForSeconds oneSecond = new WaitForSeconds(1f);
        WaitUntil waitUntilPassiveIncomeActivation = new WaitUntil(() => passiveIncomeActive);

        while (true)
        {
            if (passiveIncomeActive)
            {
                yield return oneSecond;
                IncrementRNA_Passive();
            }
            else
            {
                yield return waitUntilPassiveIncomeActivation;
            }
        }
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            passiveIncomeActive = false;
        }
        else
        {
            passiveIncomeActive = true;
        }
    }

    void SaveData()
    {
        PlayerPrefs.SetString(PrefKeys.RNA_COUNT, currentRNA.ToString());

        PlayerPrefs.SetInt(PrefKeys.RNA_RATE_LEVEL_ACTIVE, currentRNARate_Active_Level);
        PlayerPrefs.SetInt(PrefKeys.RNA_RATE_LEVEL_PASSIVE, currentRNARate_Passive_Level);
        PlayerPrefs.SetInt(PrefKeys.RNA_RATE_LEVEL_OFFLINE, currentRNARate_Offline_Level);

        PlayerPrefs.SetString(PrefKeys.OFFLINE_TIME, System.DateTime.Now.ToString());//System.Globalization.CultureInfo.InvariantCulture));
    }

    void InitData()
    {
        currentRNA = ulong.Parse(PlayerPrefs.GetString(PrefKeys.RNA_COUNT, gameSettings.startingRNACount.ToString()));

        currentRNARate_Active_Level = PlayerPrefs.GetInt(PrefKeys.RNA_RATE_LEVEL_ACTIVE, 0);
        currentRNARate_Passive_Level = PlayerPrefs.GetInt(PrefKeys.RNA_RATE_LEVEL_PASSIVE, 0);
        currentRNARate_Offline_Level = PlayerPrefs.GetInt(PrefKeys.RNA_RATE_LEVEL_OFFLINE, 0);

        currentRNARate_Active = Mathf.Lerp(gameSettings.startingRNARate_Active, gameSettings.endingRNARate_Active,
            gameSettings.rnaRateUpgradeCurve_Active.Evaluate((float)currentRNARate_Active_Level / (float)gameSettings.rnaRateUpgradeResolution_Active));
        currentRNARate_Passive = gameSettings.rnaRateUpgradeMultiplier_Passive * currentRNARate_Passive_Level + gameSettings.startingRNARate_Passive;
        currentRNARate_Offline = gameSettings.rnaRateUpgradeMultiplier_Offline * currentRNARate_Offline_Level + gameSettings.startingRNARate_Offline;

        currentRNARateUpgrade_Active_Cost = gameSettings.baseCostRNARateUpgrade_Active *
            Mathf.Pow(gameSettings.rnaRateCostMultiplier_Active, currentRNARate_Active_Level);
        currentRNARateUpgrade_Passive_Cost = gameSettings.baseCostRNARateUpgrade_Passive *
            Mathf.Pow(gameSettings.rnaRateCostMultiplier_Passive, currentRNARate_Passive_Level);
        currentRNARateUpgrade_Offline_Cost = gameSettings.baseCostRNARateUpgrade_Offline *
            Mathf.Pow(gameSettings.rnaRateCostMultiplier_Offline, currentRNARate_Offline_Level);

        gameStartTime = System.DateTime.Now;
        System.DateTime lastOfflineMark;

        System.DateTime temp;
        if (System.DateTime.TryParse(PlayerPrefs.GetString(PrefKeys.OFFLINE_TIME, ""),out temp))
            Debug.Log("time: " + temp); 

        if (System.DateTime.TryParse(PlayerPrefs.GetString(PrefKeys.OFFLINE_TIME, ""),
           out lastOfflineMark))
        {
            System.TimeSpan offlineTime = gameStartTime - lastOfflineMark;
            double totalSecondsOffline = offlineTime.TotalSeconds;
            ulong offlineIncome = (ulong)(totalSecondsOffline * currentRNARate_Offline);

            Debug.Log("Last Offline Time: " + lastOfflineMark);
            Debug.Log("Game Start Time: " + gameStartTime);
            Debug.Log("Total Seconds Offline: " + (int)totalSecondsOffline);

            collect_Button.onClick.AddListener(() =>
            {
                analytics.OfflineIncomeCollect((float)offlineIncome);
                collect_Button.enabled = false;
                LeanTween.cancel(collect_Button.gameObject);
                AddRNABonus(offlineIncome > gameSettings.maxOfflineEarning ? gameSettings.maxOfflineEarning : offlineIncome, collect_Button.transform,
                        () =>
                        {
                            LeanTween.scale(welcomeBack_Panel, Vector3.zero, 0.5f).setEase(LeanTweenType.easeInBack);
                            OnGameplayStart();
                        });
            });
            LeanTween.scale(welcomeBack_Panel, Vector3.one, 0.5f).setEase(LeanTweenType.easeOutBack).setOnComplete(() =>
            {
                StartCoroutine(DataFormater.CountUpAnimation(0, offlineIncome, offlineIncome_Text, 1f, () =>
                    {
                        LeanTween.scale(collect_Button.gameObject, Vector3.one, 0.5f).setEase(LeanTweenType.easeOutBack).setOnComplete(() =>
                        {
                            LeanTween.scale(collect_Button.gameObject, Vector3.one * 1.1f, 0.5f).setLoopPingPong();
                        });
                    }));
            });
        }
        else
        {
            OnGameplayStart();
        }

        
        UpdateRNACountUI();
        UpdateRNAParamUI();

        DataSaver.Instance.OnSaveData += SaveData;
    }

    void IncrementRNA_Active()
    {
        sceneController.TapIncomeParticle();
        float activeRate = currentRNARate_Active;
        int portionToAddFromRate = Mathf.FloorToInt(activeRate);
        activeIncomeCache += activeRate - portionToAddFromRate;
        int portionToAddFromCache = Mathf.FloorToInt(activeIncomeCache);
        activeIncomeCache -= portionToAddFromCache;
        currentRNA += (ulong)(portionToAddFromRate + portionToAddFromCache);

        FlyingText ft = Instantiate(flyingText_Prefab, rnaActiveIncome_Button.transform);
        ft.transform.localPosition = new Vector2(-200f, 180f);
        ft.Init(DataFormater.FormattedRNACount((int)currentRNARate_Active));

        UpdateRNACountUI();
        analytics.UpdateCoin((int)currentRNA);
    }

    void IncrementRNA_Passive()
    {
        float passiveRate = currentRNARate_Passive;
        int portionToAddFromRate = Mathf.FloorToInt(passiveRate);
        passiveIncomeCache += passiveRate - portionToAddFromRate;
        int portionToAddFromCache = Mathf.FloorToInt(passiveIncomeCache);
        passiveIncomeCache -= portionToAddFromCache;
        currentRNA += (ulong)(portionToAddFromRate + portionToAddFromCache);

        UpdateRNACountUI();
    }

    void UpdateRNACountUI()
    {
        rnaCount_Text.text = DataFormater.FormattedRNACount(currentRNA);
    }

    void UpdateRNAParamUI()
    {
        rnaRateActive_Text.text = $"{DataFormater.FormattedRNACount((int)currentRNARate_Active)}/tap";
        rnaRatePassive_Text.text = $"{DataFormater.FormattedRNACount((int)currentRNARate_Passive)}/sec";

        //Upgrade Panel:
        float nextRNARateActive = Mathf.Lerp(gameSettings.startingRNARate_Active, gameSettings.endingRNARate_Active,
            gameSettings.rnaRateUpgradeCurve_Active.Evaluate((float)(currentRNARate_Active_Level + 1) / (float)gameSettings.rnaRateUpgradeResolution_Active));
        rnaRateNextUpgrade_Active_Text.text = $"{DataFormater.FormattedRNACount((int)currentRNARate_Active)}/tap";
        float nextRNARatePassive = gameSettings.rnaRateUpgradeMultiplier_Passive * (currentRNARate_Passive_Level + 1) + gameSettings.startingRNARate_Passive;
        rnaRateNextUpgrade_Passive_Text.text = $"{DataFormater.FormattedRNACount((int)currentRNARate_Passive)}/sec";
        float nextRNARateOffline = gameSettings.rnaRateUpgradeMultiplier_Offline * (currentRNARate_Offline_Level + 1) + gameSettings.startingRNARate_Offline;
        rnaRateNextUpgrade_Offline_Text.text = $"{DataFormater.FormattedRNACount((int)currentRNARate_Offline)}/sec";

        rnaRateActiveUpgradeCost_Text.text = $"{DataFormater.FormattedRNACount((int)currentRNARateUpgrade_Active_Cost)}";
        rnaRatePassiveUpgradeCost_Text.text = $"{DataFormater.FormattedRNACount((int)currentRNARateUpgrade_Passive_Cost)}";
        rnaRateOfflineUpgradeCost_Text.text = $"{DataFormater.FormattedRNACount((int)currentRNARateUpgrade_Offline_Cost)}";
    }

    void UpgradeRNARate_Active()
    {
        if (currentRNA >= currentRNARateUpgrade_Active_Cost)
        {
            SpendRNA((int)currentRNARateUpgrade_Active_Cost, rnaRateActiveUpgradeCostBG_Img.transform);
            currentRNARate_Active_Level++;

            float previousRNARate = currentRNARate_Active;
            currentRNARate_Active = Mathf.Lerp(gameSettings.startingRNARate_Active, gameSettings.endingRNARate_Active,
            gameSettings.rnaRateUpgradeCurve_Active.Evaluate((float)currentRNARate_Active_Level / (float)gameSettings.rnaRateUpgradeResolution_Active));
            float rnaRateDelta = currentRNARate_Active - previousRNARate;
            currentRNARateUpgrade_Active_Cost = gameSettings.baseCostRNARateUpgrade_Active *
            Mathf.Pow(gameSettings.rnaRateCostMultiplier_Active, currentRNARate_Active_Level);

            FlyingText ft = Instantiate(flyingText_Prefab, rnaRateActive_Text.transform);
            ft.transform.localPosition = new Vector2(10f, 25f);
            ft.Init(DataFormater.FormattedRNACount((int)rnaRateDelta));

            UpdateRNAParamUI();

            if (currentRNARate_Active_Level > gameSettings.rnaRateUpgradeResolution_Active)
            {
                rnaRateActiveUpgrade_Button.interactable = false;
            }

            analytics.EventButtonPress("Active RNA", currentRNARateUpgrade_Active_Cost);
        }
        else
        {
            InsufficientFundsError(rnaRateActiveUpgradeCostBG_Img);
        }
    }

    public void UpgradeRNARate_Passive()
    {
        if (currentRNA >= currentRNARateUpgrade_Passive_Cost)
        {
            SpendRNA((int)currentRNARateUpgrade_Passive_Cost, rnaRatePassiveUpgradeCostBG_Img.transform);
            currentRNARate_Passive_Level++;

            float previousRNARate = currentRNARate_Passive;
            currentRNARate_Passive = gameSettings.rnaRateUpgradeMultiplier_Passive * currentRNARate_Passive_Level + gameSettings.startingRNARate_Passive;
            float rnaRateDelta = currentRNARate_Passive - previousRNARate;
            currentRNARateUpgrade_Passive_Cost = gameSettings.baseCostRNARateUpgrade_Passive *
            Mathf.Pow(gameSettings.rnaRateCostMultiplier_Passive, currentRNARate_Passive_Level);

            FlyingText ft = Instantiate(flyingText_Prefab, rnaRatePassive_Text.transform);
            ft.transform.localPosition = new Vector2(10f, 25f);
            ft.Init(DataFormater.FormattedRNACount((int)rnaRateDelta));

            UpdateRNAParamUI();

            analytics.EventButtonPress("Passive RNA", currentRNARateUpgrade_Passive_Cost);
        }
        else
        {
            InsufficientFundsError(rnaRatePassiveUpgradeCostBG_Img);
        }
    }

    void UpgradeRNARate_Offline()
    {
        if (currentRNA >= currentRNARateUpgrade_Offline_Cost)
        {
            SpendRNA((int)currentRNARateUpgrade_Offline_Cost, rnaRateOfflineUpgradeCostBG_Img.transform);
            currentRNARate_Offline_Level++;

            currentRNARate_Offline = gameSettings.rnaRateUpgradeMultiplier_Offline * currentRNARate_Offline_Level + gameSettings.startingRNARate_Offline;
            currentRNARateUpgrade_Offline_Cost = gameSettings.baseCostRNARateUpgrade_Offline *
            Mathf.Pow(gameSettings.rnaRateCostMultiplier_Offline, currentRNARate_Offline_Level);

            UpdateRNAParamUI();

            analytics.EventButtonPress("Offline RNA", currentRNARateUpgrade_Offline_Cost);
        }
        else
        {
            InsufficientFundsError(rnaRateOfflineUpgradeCostBG_Img);
        }
    }

    public void SpendRNA(int spending, Transform ui)
    {
        currentRNA -= (ulong)spending;
        spentRNA += (ulong)spending;

        FlyingText ft = Instantiate(flyingText_Prefab, ui);
        ft.transform.localPosition = new Vector2(-250f, -50f);
        ft.Init(DataFormater.FormattedRNACount(spending), -1);

        UpdateRNACountUI();
    }

    public void AddRNABonus(ulong rna, Transform origin, System.Action OnComplete = null)
    {
        currentRNA += rna;

        int dnas = Random.Range(10, 15);
        for (int i = 0; i < dnas; i++)
        {
            Image dnaObject = Instantiate(dnaFly_Prefab, origin);
            dnaObject.transform.localPosition = Vector3.zero;
            dnaObject.transform.localEulerAngles = new Vector3(0f, 0f, Random.Range(0f, 180f));

            Vector2 direction = Random.insideUnitCircle.normalized;
            float rotationAngle = Random.Range(30f, 179f);
            LeanTween.rotateLocal(dnaObject.gameObject, Vector3.forward * Mathf.Sign(Random.Range(-1f, 1f)) * rotationAngle, 1f).setEase(LeanTweenType.easeOutCubic);
            LeanTween.moveLocal(dnaObject.gameObject, direction * Random.Range(100f, 200f), 0.75f).setEase(LeanTweenType.easeOutCubic).
                setOnComplete(() =>
                {
                    Vector3 targetPos = rnaCount_Text.transform.position;
                    LeanTween.move(dnaObject.gameObject, targetPos, 0.5f).setEase(LeanTweenType.easeInOutCubic).
                    setOnComplete(() =>
                    {
                        Destroy(dnaObject.gameObject);

                        FlyingText ft = Instantiate(flyingText_Prefab, rnaCount_Text.transform);
                        ft.transform.localPosition = new Vector2(50f, 50f);
                        ft.Init(DataFormater.FormattedRNACount(rna), duration: 0.8f, fontSize: 50f);

                        UpdateRNACountUI();

                        OnComplete?.Invoke();
                    });
                });
        }
    }

    void InsufficientFundsError(Image image)
    {
        image.color = Color.red;
        LeanTween.delayedCall(0.1f, () => image.color = Color.white);
    }
}

public class DataFormater
{
    public static string FormattedRNACount(int rnaCount, int decimalPlaces = 2)
    {
        if (((long)rnaCount) >= 1000000000000000)
        {
            return $"{((double)rnaCount / 1000000000000000.0).ToString($"F{decimalPlaces}")}Q";
        }
        else if (((long)rnaCount) >= 1000000000000)
        {
            return $"{((double)rnaCount / 1000000000000.0).ToString($"F{decimalPlaces}")}T";
        }
        else if (rnaCount >= 1000000000)
        {
            return $"{((double)rnaCount / 1000000000.0).ToString($"F{decimalPlaces}")}B";
        }
        else if (rnaCount >= 1000000)
        {
            return $"{((double)rnaCount / 1000000.0).ToString($"F{decimalPlaces}")}M";
        }
        else if (rnaCount >= 1000)
        {
            return $"{((double)rnaCount / 1000.0).ToString($"F{decimalPlaces}")}K";
        }
        else
        {
            return rnaCount.ToString();
        }
    }

    public static string FormattedRNACount(ulong rnaCount, int decimalPlaces = 2)
    {
        if (rnaCount >= 1000000000000000)
        {
            return $"{((double)rnaCount / 1000000000000000.0).ToString($"F{decimalPlaces}")}Q";
        }
        else if (rnaCount >= 1000000000000)
        {
            return $"{((double)rnaCount / 1000000000000.0).ToString($"F{decimalPlaces}")}T";
        }
        else if (rnaCount >= 1000000000)
        {
            return $"{((double)rnaCount / 1000000000.0).ToString($"F{decimalPlaces}")}B";
        }
        else if (rnaCount >= 1000000)
        {
            return $"{((double)rnaCount / 1000000.0).ToString($"F{decimalPlaces}")}M";
        }
        else if (rnaCount >= 1000)
        {
            return $"{((double)rnaCount / 1000.0).ToString($"F{decimalPlaces}")}K";
        }
        else
        {
            return rnaCount.ToString();
        }
    }

    public static IEnumerator CountUpAnimation(int startVal, int endVal, TMP_Text textComponent, float duration, System.Action OnComplete = null)
    {
        int deltaVal = endVal - startVal;
        int perFrameUpdate = (int)(((float)deltaVal / (float)duration) * Time.deltaTime);
        int currentVal = startVal;
        float progress = 0f;
        float startTime = Time.time;
        float elapsedTime = 0f;
        while (elapsedTime < duration)
        {
            yield return null;
            elapsedTime += Time.deltaTime;
            currentVal += perFrameUpdate;
            if (currentVal < endVal)
            {
                textComponent.text = FormattedRNACount(currentVal); 
            }
        }

        textComponent.text = FormattedRNACount(endVal);

        OnComplete?.Invoke();
    }

    public static IEnumerator CountUpAnimation(ulong startVal, ulong endVal, TMP_Text textComponent, float duration, System.Action OnComplete = null)
    {
        ulong deltaVal = endVal - startVal;
        int perFrameUpdate = (int)(((double)deltaVal / (double)duration) * Time.deltaTime);
        ulong currentVal = startVal;
        float progress = 0f;
        float startTime = Time.time;
        float elapsedTime = 0f;
        while (elapsedTime < duration)
        {
            yield return null;
            elapsedTime += Time.deltaTime;
            currentVal += (ulong)perFrameUpdate;
            if (currentVal < endVal)
            {
                textComponent.text = FormattedRNACount(currentVal);
            }
        }

        textComponent.text = FormattedRNACount(endVal);

        OnComplete?.Invoke();
    }

    public static IEnumerator CountUpTimeAnimation(int timeInSeconds, TMP_Text textComponent, float duration, System.Action OnComplete = null)
    {
        int perFrameUpdate = (int)(((float)timeInSeconds / duration) * Time.deltaTime);
        perFrameUpdate = perFrameUpdate >= 1 ? perFrameUpdate : 1;
        int currentVal = 0;
        float elapsedTime = 0f;
        while (elapsedTime < duration)
        {
            yield return null;
            elapsedTime += Time.deltaTime;
            currentVal += perFrameUpdate;
            if (currentVal < timeInSeconds)
            {
                textComponent.text = FormattedTime(currentVal);
            }
        }

        textComponent.text = FormattedTime(timeInSeconds);

        OnComplete?.Invoke();
    }

    static string FormattedTime(int seconds)
    {
        int secs = seconds % 60;
        int mins = seconds / 60;
        int hrs = mins / 60;
        mins = mins % 60;

        return $"{hrs.ToString("00")}hrs : {mins.ToString("00")}mins : {secs.ToString("00")}secs";
    }
}
