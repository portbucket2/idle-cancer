﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class CancerController : MonoBehaviour
{
    public static CancerController Instance { get; private set; }
    private const int CANCER_STAGES = 4;
    public float cancerProgress_Overall { get; private set; }
    public float cancerProgress_ThisStage { get; private set; }

    SceneController sceneController;
    GameController gameController;
    bool cellSplit;

    public CancerProperty[] cancerProperties;

    public int currentCancerStage { get; private set; } = 1;

    [SerializeField] TMP_Text cancerStage_Text;
    [SerializeField] Image cancerProgressBar;
    [SerializeField] TMP_Text cancerProgress_Text;

    [SerializeField] public int[] maxMutationLevels;

    [SerializeField] GameObject cancerUpstage_Panel;
    [SerializeField] Button cancerUpstagedContinute_Button;
    [SerializeField] TMP_Text cancerUpstagePanelStage_Text;

    [SerializeField] GameObject deathPanel;
    [SerializeField] Button deathPanelContinue_Button;
    [SerializeField] TMP_Text deathPanelPlayTime_Text;
    [SerializeField] TMP_Text deathPanelPointsEarned_Text;

    [SerializeField] ulong[] rnaBonusOnUpstage;
    [SerializeField] TMP_Text cancerUpstagePanelRNABonus_Text;

    public int maxMutationLevel { get { return maxMutationLevels[currentCancerStage - 1]; } }
    AnalyticsController analytics;
    private void Awake()
    {
        Instance = this;

        if (PlayerPrefs.GetInt(PrefKeys.GAME_COMPLETED, 0) != 0)
        {
            StartGameAfresh();
        }
    }

    void InitData()
    {
        currentCancerStage = PlayerPrefs.GetInt(PrefKeys.CANCER_STAGE, 1);
        cancerStage_Text.text = currentCancerStage < 5 ? $"STAGE {currentCancerStage}" : "STAGE DEAD";

        foreach (CancerProperty cancer in cancerProperties)
        {
            cancer.Init();
        }

        UpdateCancerProgress();
        //sceneController.LoadVisualStage(currentCancerStage);
        DataSaver.Instance.OnSaveData += SaveData;
    }

    void UpgradeCancerStage()
    {
        currentCancerStage++;

        if (currentCancerStage > CANCER_STAGES)
        {
            cancerStage_Text.text = "STAGE DEAD";
            ShowGameCompletePrompt();
        }
        else
        {
            ulong rnaBonus = rnaBonusOnUpstage[currentCancerStage - 2];

            foreach (CancerProperty cancer in cancerProperties)
            {
                cancer.ResetPrpertyOnStageChange();
            }

            ShowCancerUpstagedPrompt(rnaBonus);
        }

        StageChangeVisual();
    }

    void ShowCancerUpstagedPrompt(ulong rnaBonus)
    {
        cancerUpstagePanelStage_Text.text = $"STAGE {currentCancerStage}";
        cancerUpstagePanelRNABonus_Text.text = DataFormater.FormattedRNACount(rnaBonus);

        cancerUpstagedContinute_Button.enabled = true;
        cancerUpstagedContinute_Button.onClick.RemoveAllListeners();
        cancerUpstagedContinute_Button.onClick.AddListener(() =>
        {
            cancerUpstagedContinute_Button.enabled = false;
            cancerUpstagedContinute_Button.transform.localScale = Vector3.one;
            LeanTween.cancel(cancerUpstage_Panel);
            LeanTween.cancel(cancerUpstagedContinute_Button.gameObject);

            RNAController.Instance.AddRNABonus(rnaBonus, cancerUpstagedContinute_Button.transform, () => 
            {
                LeanTween.scale(cancerUpstage_Panel, Vector3.zero, 0.5f).setEase(LeanTweenType.easeInBack);
                cancerStage_Text.text = $"STAGE {currentCancerStage}";
                foreach (CancerProperty cancer in cancerProperties)
                {
                    cancer.ResetPropertyVisualOnStageContinue();
                }
            });
        });

        LeanTween.scale(cancerUpstage_Panel, Vector3.one, 0.5f).setEase(LeanTweenType.easeOutBack).
            setOnComplete(() => LeanTween.scale(cancerUpstagedContinute_Button.gameObject, Vector3.one * 1.1f, 0.5f).setLoopPingPong());
    }

    public void UpdateCancerProgress()
    {
        CalculateCancerProgress();

        cancerProgressBar.fillAmount = cancerProgress_Overall;
        cancerProgress_Text.text = $"{(cancerProgressBar.fillAmount * 100f).ToString("F2")}%";

        CancerController.Instance.UpdateVisualProgress();

        if (cancerProgress_ThisStage >= 1f)
        {
            UpgradeCancerStage();
        }
    }

    void CalculateCancerProgress()
    {
        int properties = cancerProperties.Length;

        int sumOfPropertyLevelsThisStage = 0;
        for (int i = 0; i < properties; i++)
        {
            sumOfPropertyLevelsThisStage += cancerProperties[i].level_ThisStage;
        }
        cancerProgress_ThisStage = Mathf.Clamp01((float)sumOfPropertyLevelsThisStage / (float)(maxMutationLevel * properties));

        cancerProgress_Overall = Mathf.Clamp01((float)((currentCancerStage - 1) + cancerProgress_ThisStage) / (float)CANCER_STAGES);
    }

    //Called with each mutation tap
    void UpdateVisualProgress()
    {
        switch (currentCancerStage)
        {
            case 1:
                if (!cellSplit)
                {
                    sceneController.SplitCell();
                    cellSplit = true; 
                }
                break;
            case 2:
                sceneController.Bubble();
                break;
            case 3:
                sceneController.InfectColon();
                break;
            case 4:
                sceneController.InfectLiver();
                break;
            default:
                break;
        }
    }

    //Called once on stage change
    void StageChangeVisual()
    {
        gameController.GetCharacter().LoadAsLevel(currentCancerStage);
        Debug.Log("current cancer stage: " + currentCancerStage);
        switch (currentCancerStage)
        {
            case 1:
                analytics.LevelStarted();
                break;
            case 2:
                sceneController.CancerBubbleStage();
                analytics.LevelCompleted();
                analytics.LevelStarted();
                break;
            case 3:
                sceneController.CancerCameraChange();
                analytics.LevelCompleted();
                analytics.LevelStarted();
                break;
            case 4:
                sceneController.CancerTransmissionStage();
                analytics.LevelCompleted();
                break;
            default:
                break;
        }
    }


    // Use this for initialization
    void Start()
    {
        sceneController = GameController.GetController().GetSceneController();
        gameController = GameController.GetController();
        InitData();
        analytics = AnalyticsController.GetController();
        analytics.LevelStarted();
    }

    void SaveData()
    {
        PlayerPrefs.SetInt(PrefKeys.CANCER_STAGE, currentCancerStage);

        foreach (CancerProperty cancer in cancerProperties)
        {
            cancer.SavePropertyLevel();
        }
    }

    void ShowGameCompletePrompt()
    {
        sceneController.SkullParticle();
        PlayerPrefs.SetInt(PrefKeys.GAME_COMPLETED, 1);

        deathPanelContinue_Button.onClick.AddListener(() =>
        {
            deathPanelContinue_Button.enabled = false;
            deathPanelContinue_Button.transform.localScale = Vector3.one;
            deathPanel.SetActive(false);
            analytics.GameEnd();

            StartGameAfresh();
        });

        deathPanel.SetActive(true);
        deathPanelContinue_Button.enabled = true;

        LeanTween.scale(deathPanel, Vector3.one, 0.5f).setEase(LeanTweenType.easeOutBack).setOnComplete(() =>
        {
            StartCoroutine(DataFormater.CountUpTimeAnimation((int)DataSaver.Instance.GetPlayTime(), deathPanelPlayTime_Text, 2f, () =>
            {
                StartCoroutine(DataFormater.CountUpAnimation(0, DataSaver.Instance.GetTotalRNAPoints(), deathPanelPointsEarned_Text, 1f, () =>
                {
                    LeanTween.scale(deathPanelContinue_Button.gameObject, Vector3.one, 0.5f).setEase(LeanTweenType.easeOutBack).setOnComplete(() =>
                    {
                        LeanTween.scale(deathPanelContinue_Button.gameObject, Vector3.one * 1.1f, 0.5f).setLoopPingPong();
                    });
                }));
            }));
        });
    }

    void StartGameAfresh()
    {
        LeanTween.cancelAll();
        PlayerPrefs.DeleteAll();
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
    }
}