public struct PrefKeys
{
    public const string RNA_COUNT = "RNA_Count";
    public const string RNA_RATE_LEVEL_ACTIVE = "RNA_Rate_Active";
    public const string RNA_RATE_LEVEL_PASSIVE = "RNA_Rate_Passive";
    public const string RNA_RATE_LEVEL_OFFLINE = "RNA_Rate_Offline";
    public const string OFFLINE_TIME = "Offline_Time";
    public const string CANCER_STAGE = "Cancer_Stage";
    public const string PLAY_TIME = "Play_Time";
    public const string GAME_COMPLETED = "Game_Completed";
    public const string TOTAL_RNA_SPENDING = "Total_RNA_Spending";
}
