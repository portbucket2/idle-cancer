using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonEnlarger : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] float enlargeMultiplier;

    public void OnPointerDown(PointerEventData eventData)
    {
        transform.localScale = Vector3.one * enlargeMultiplier;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        transform.localScale = Vector3.one;
    }

    [Header("Auto Clicker:")]
    [SerializeField] bool autoClick = false;
    [SerializeField] float autoClickDelay = -1;
    bool pointerOverButton;

    void Start()
    {
        if (autoClick)
        {
            StartCoroutine(AutoClicker());
        }
    }

    IEnumerator AutoClicker()
    {
        WaitForSeconds delay = null;

        if (autoClickDelay > 0f)
        {
            delay = new WaitForSeconds(autoClickDelay);
        }

        while (true)
        {
            if (autoClick && pointerOverButton)
            {
                Button button = GetComponent<Button>();
                if (button.interactable)
                {
                    button.onClick.Invoke();
                }
            }
            yield return delay;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        pointerOverButton = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        pointerOverButton = false;
    }
}
