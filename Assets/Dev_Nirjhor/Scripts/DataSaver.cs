﻿using System.Collections;
using UnityEngine;

public class DataSaver : MonoBehaviour
{
    public static DataSaver Instance { get; private set; }

    public float saveInterval_Seconds;

    float storedPlayTime;

    ulong storedRNASpending = 0;

    private void Awake()
    {
        Instance = this;
    }

    public event System.Action OnSaveData;

    AnalyticsController analytics;

    private void Start()
    {
        analytics = AnalyticsController.GetController();
        storedPlayTime = PlayerPrefs.GetFloat(PrefKeys.PLAY_TIME, 0f);
        storedRNASpending = ulong.Parse(PlayerPrefs.GetString(PrefKeys.TOTAL_RNA_SPENDING, "0"));
        StartCoroutine(PeriodicDataSaver());
    }

    IEnumerator PeriodicDataSaver()
    {
        WaitForSeconds saveDelay = new WaitForSeconds(saveInterval_Seconds);
        while (true)
        {
            yield return saveDelay;
            SaveData();
        }
    }

    void SaveData()
    {
        PlayerPrefs.SetString(PrefKeys.TOTAL_RNA_SPENDING, (storedRNASpending + RNAController.Instance.spentRNA).ToString());
        PlayerPrefs.SetFloat(PrefKeys.PLAY_TIME, storedPlayTime + Time.realtimeSinceStartup);

        OnSaveData?.Invoke();

        analytics.SaveGame();

        Debug.Log("DATA SAVED");
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            SaveData();
        }
    }

    private void OnApplicationQuit()
    {
        SaveData();
    }

    public float GetPlayTime()
    {
        return storedPlayTime + Time.realtimeSinceStartup;
    }

    public ulong GetTotalRNAPoints()
    {
        return storedRNASpending + RNAController.Instance.spentRNA + RNAController.Instance.currentRNA;
    }
}