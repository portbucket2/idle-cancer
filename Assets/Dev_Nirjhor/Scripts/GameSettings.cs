﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "New Game Settings", menuName = "ScriptableObjects/Game Settings")]
public class GameSettings : ScriptableObject
{
    [Header("Base Stats:")]
    public ulong startingRNACount;
    public int startingRNARate_Active;
    public int endingRNARate_Active;
    public float startingRNARate_Passive;
    public float startingRNARate_Offline;

    [Header("Base Costs:")]
    public int baseCostRNARateUpgrade_Active;
    public int baseCostRNARateUpgrade_Passive;
    public int baseCostRNARateUpgrade_Offline;

    [Header("RNA Upgrades:")]
    public int rnaRateUpgradeResolution_Active;
    public AnimationCurve rnaRateUpgradeCurve_Active;

    public float rnaRateUpgradeMultiplier_Passive;
    public float rnaRateUpgradeMultiplier_Offline;

    [Header("RNA Costs:")]
    [Range(1.07f, 1.15f)] public float rnaRateCostMultiplier_Active;
    [Range(1.07f, 1.15f)] public float rnaRateCostMultiplier_Passive;
    [Range(1.07f, 1.15f)] public float rnaRateCostMultiplier_Offline;

    [Header("Ceiling:")]
    public ulong maxOfflineEarning;
}