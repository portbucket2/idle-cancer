using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour
{
    [Header("buttons")]
    [SerializeField] Button tapButton;
    [SerializeField] Button upgradeButton;
    [SerializeField] Button growthButton;
    [SerializeField] Button damageButton;
    [SerializeField] Button strengthButton;
    [SerializeField] Button firstMessgaeButton;

    [Header("panels")]
    [SerializeField] GameObject firstMessage;
    [SerializeField] GameObject secondMessage;
    [SerializeField] GameObject thirdMessage;
    [SerializeField] GameObject fourthMessage;

    [Header("Debug: ")]
    [SerializeField] int tutorialCount = 0;
    [SerializeField] bool tutorialComplete = false;

    GameManager gameManager;
    
    void Start()
    {
        gameManager = GameManager.GetManager();
        tutorialComplete = gameManager.HasTutorialSeen();
        if (!tutorialComplete)
        {
            StartTutorial();
        }
        else
        {
            AllEnabled();
        }

        firstMessgaeButton.onClick.AddListener(delegate {
            NextTutorial();
	    });
    }

    void StartTutorial()
    {
        FirstTutorial();
    }

    void AllEnabled()
    {
        tapButton.interactable = true;
        upgradeButton.interactable = true;
        growthButton.interactable = true;
        damageButton.interactable = true;
        strengthButton.interactable = true;

        firstMessage.SetActive(false);
        secondMessage.SetActive(false);
        thirdMessage.SetActive(false);
        fourthMessage.SetActive(false);
    }
    void FirstTutorial() {
        // message
        tapButton.interactable = false;
        upgradeButton.interactable = false;
        growthButton.interactable = false;
        damageButton.interactable = false;
        strengthButton.interactable = false;

        firstMessage.SetActive(true);
        secondMessage.SetActive(false);
        thirdMessage.SetActive(false);
        fourthMessage.SetActive(false);
    }
    void SecondTutorial() {
        // show tap button
        tapButton.interactable = true;
        upgradeButton.interactable = false;
        growthButton.interactable = false;
        damageButton.interactable = false;
        strengthButton.interactable = false;

        firstMessage.SetActive(false);
        secondMessage.SetActive(true);
        thirdMessage.SetActive(false);
        fourthMessage.SetActive(false);
    }
    void ThirdTutorial() {
        //show mutate buttons
        tapButton.interactable = false;
        upgradeButton.interactable = false;
        growthButton.interactable = true;
        damageButton.interactable = true;
        strengthButton.interactable = true;

        firstMessage.SetActive(false);
        secondMessage.SetActive(false);
        thirdMessage.SetActive(true);
        fourthMessage.SetActive(false);
    }
    void FourthTutorial() {
        //enable upgrade
        //and end tutorial
        tapButton.interactable = true;
        upgradeButton.interactable = false;
        growthButton.interactable = true;
        damageButton.interactable = true;
        strengthButton.interactable = true;

        firstMessage.SetActive(false);
        secondMessage.SetActive(false);
        thirdMessage.SetActive(false);
        fourthMessage.SetActive(false);
    }
    
    public void FirstStageComplete()
    {
        //fifth message
        Debug.Log("fifth message!!");

        if (tutorialComplete)
            return;

        tapButton.interactable = false;
        upgradeButton.interactable = true;
        growthButton.interactable = false;
        damageButton.interactable = false;
        strengthButton.interactable = false;

        firstMessage.SetActive(false);
        secondMessage.SetActive(false);
        thirdMessage.SetActive(false);

        fourthMessage.SetActive(true);
    }

    public void NextTutorial()
    {
        if (tutorialComplete)
            return;

        tutorialCount++;
        switch (tutorialCount)
        {
            case 1:
                SecondTutorial();
                break;
            case 2:
                ThirdTutorial();
                break;
            case 3:
                FourthTutorial();
                break;
            default:
                break;
        }
    }
    public void TutorialComplete()
    {
        if (tutorialComplete)
            return;

        tutorialComplete = true;
        gameManager.TutorialSeen();
        AllEnabled();
    }
    
}
