using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController gameController;
    private void Awake()
    {
        gameController = this;
    }
    public static GameController GetController() { return gameController; }

    [SerializeField] InputController inputController;
    [SerializeField] Camera mainCamera;
    [SerializeField] UIController uIController;
    [SerializeField] SceneController sceneController;
    [SerializeField] CameraController cellCameraController;
    [SerializeField] CameraController organCameraController;
    [SerializeField] CharacterActivity characterActivity;
    [SerializeField] TutorialController tutorialController;


    [Header("Debug: ")]
    [SerializeField] LevelData levelData;
    [SerializeField] int currentLevel = 0;
    [SerializeField] GameDataSheet data;

    GameManager gameManager;
    //AnalyticsController analytics;


    void Start()
    {
        gameManager = GameManager.GetManager();
        //analytics = AnalyticsController.GetController();
        //analytics.LevelStarted();

        //data = gameManager.GetDataSheet();
        //currentLevel = gameManager.GetlevelCount();
        levelData = new LevelData();
        levelData = data.levelDatas[currentLevel];

    }

    public void EventOne() { Debug.Log("event 1 complete!!"); }
    public void EventTwo() { Debug.Log("event 2 complete!!"); }
    public void EventThree() { Debug.Log("event 3 complete!!"); }

    public UIController GetUI() { return uIController; }
    public InputController GetInputController() { return inputController; }


    public void LevelComplete()
    {
        uIController.LevelComplete();
        //analytics.LevelCompleted();
    }
    public void LevelFailed()
    {
        //analytics.LevelFailed();
    }
    public int CurrentLevel() { return currentLevel; }
    public GameDataSheet GetData() { return data; }
    public SceneController GetSceneController() { return sceneController; }
    public CameraController GetCellCamera() { return cellCameraController; }
    public CameraController GetOrganCamera() { return organCameraController; }
    public CharacterActivity GetCharacter() { return characterActivity; }
    public TutorialController GetTutorial() { return tutorialController; }


    public void StartOrganCamera()
    {
        cellCameraController.gameObject.SetActive(false);
        organCameraController.gameObject.SetActive(true);
        organCameraController.Intro();
    }
}
