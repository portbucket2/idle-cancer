using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
//using AppLovinMax;

public class MaxSDKInitializer : MonoBehaviour
{
#if !UNITY_IOS
    staic bool  hasConsent = true;
#else
    static bool hasConsent = false;
#endif
    // Start is called before the first frame update
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void Initializer()
    {
        Debug.Log("<color=#00ffff>Max SDK initialized</color>");
        MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) =>
        {

#if UNITY_IOS
            if (MaxSdkUtils.CompareVersions(UnityEngine.iOS.Device.systemVersion, "14.5") != MaxSdkUtils.VersionComparisonResult.Lesser)
            {
                Debug.Log("<color=#00ffff>proper version</color>");
                // Note that App transparency tracking authorization can be checked via `sdkConfiguration.AppTrackingStatus` for Unity Editor and iOS targets
                // 1. Set Facebook ATE flag here, THEN
                hasConsent = (sdkConfiguration.AppTrackingStatus == MaxSdkBase.AppTrackingStatus.Authorized);
            }
            else
            {
                Debug.Log("<color=#00ffff>Old Version </color>");
                hasConsent = true;
            }
#else
                hasConsent = true;
#endif
            Debug.Log("<color=#00ffff>Max Initialized</color>");
            OtherInit();
        };
        MaxSdk.SetVerboseLogging(false);
        MaxSdk.SetSdkKey("Lzi5VR_J50y55PM5ctwAwALT5d9g1CKMhT1TF0naOa4fSUn98Vd6rXsvAp4I3A-5LaPvNk4RSvKe5fesxKhRzh");
        MaxSdk.InitializeSdk();
    }

    static void OtherInit()
    {
        Debug.LogFormat("other Init initialized: <color=#00ffff> Concent {0} </color> : " , hasConsent);
        if (FB.IsInitialized)
        {
            OnFBInitializationConfirmed(hasConsent);
        }
        else
        {
            FB.Init(() => {
                OnFBInitializationConfirmed(hasConsent);
            });
        }
    }
    static void OnFBInitializationConfirmed(bool hasConsent)
    {
        FB.ActivateApp();
#if UNITY_IOS
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            FB.Mobile.SetAdvertiserTrackingEnabled(hasConsent);
            AudienceNetwork.AdSettings.SetAdvertiserTrackingEnabled(advertiserTrackingEnabled: hasConsent);
        }
        Debug.LogFormat("Consent={0}, will be sent if run from iOS device", hasConsent);
#endif
    }

}
