using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrganInfection : MonoBehaviour
{
    //[SerializeField] bool change;
    [SerializeField] ParticleSystem intestineWound;
    [SerializeField] ParticleSystem liverWound;

    [SerializeField] float colonLifeTime;
    [SerializeField] float size;
    [SerializeField] float liverLifeTime;

    readonly string COLONLIFETIME = "ColonLife";
    readonly string LIVERLIFETIME = "LiverLife";

    void Start()
    {
        colonLifeTime = 0f;
        liverLifeTime = 0f;
        LoadInfection();
    }

    // Update is called once per frame
    void Update()
    {
        //if (change)
        //{
        //    SetColonWoundParticle(colonLifeTime, size);
        //    change = false;
        //}
    }
    void SetColonWoundParticle(float _lifeTime, float _size)
    {
        var main = intestineWound.main;
        main.startLifetime = _lifeTime;
        main.startSize = _size;
    }
    void SetLiverWoundParticle(float _lifeTime, float _size)
    {
        var main = liverWound.main;
        main.startLifetime = _lifeTime;
        main.startSize = _size;
    }

    public void InfectColon()
    {
        colonLifeTime = PlayerPrefs.GetFloat(COLONLIFETIME, 1f);
        //Debug.Log("colon: " + colonLifeTime);

        colonLifeTime++;
        if (colonLifeTime > 90)
            colonLifeTime = 90;
        SetColonWoundParticle(colonLifeTime, size);
        //Debug.Log("colon saved at " + colonLifeTime);
        PlayerPrefs.SetFloat(COLONLIFETIME, colonLifeTime);


        Debug.Log("colon saved value " + PlayerPrefs.GetFloat(COLONLIFETIME));
    }
    public void InfectLiver()
    {
        liverLifeTime = PlayerPrefs.GetFloat(LIVERLIFETIME, 1f);
        //Debug.Log("liver: " + liverLifeTime);

        liverLifeTime++;
        if (liverLifeTime > 90)
            liverLifeTime = 90;
        SetLiverWoundParticle(liverLifeTime, size);
        //Debug.Log("liver saved at " + liverLifeTime);
        PlayerPrefs.SetFloat(LIVERLIFETIME, liverLifeTime);

        //Debug.Log("liver saved value " + PlayerPrefs.GetFloat(LIVERLIFETIME));

    }

    public void LoadInfection()
    {
        InfectColon();
        InfectLiver();

        intestineWound.Stop();
        liverWound.Stop();

        intestineWound.Play();
        liverWound.Play();
    }
}
