using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CellSplitter : MonoBehaviour
{
    [SerializeField] LineRenderer ln;
    [SerializeField] GameObject cellZero;
    [SerializeField] int connectionCount;
    [SerializeField] float midPointDistance;
    [SerializeField] float distanceBetweenTwo;
    [SerializeField] Color32 swallowCol;

    [Header("Debug :")]
    [SerializeField] GameObject cellOne;
    [SerializeField] Transform cellZeroCore;
    [SerializeField] Transform cellOneCore;
    [SerializeField] MeshRenderer rend;

    [SerializeField] List<Vector3> middlePoints;



    bool separating = false;
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    private void Awake()
    {
        //ln = GetComponent<LineRenderer>();
        cellZeroCore = cellZero.transform.GetChild(0).transform;
       
    }

    void Start()
    {
        
        rend = cellZero.GetComponent<MeshRenderer>();
        //distanceBetweenTwo = rend.bounds.extents.magnitude;
        //Debug.Log("radius : " + distanceBetweenTwo);
    }

    // Update is called once per frame
    void Update()
    {
        if (separating)
        {
            GetMiddlePoints();
            SetLineRenderer();
        }        
    }

    public void SplitTheCell()
    {
        GameObject gg = Instantiate(cellZero, this.transform);
        cellOne = gg;
        //cellZero.transform.position -= Vector3.right * 10f;
        //cellOne.transform.position += Vector3.right * 10f;
        cellOneCore = cellOne.transform.GetChild(0).transform;
        Vector3 scale = new Vector3(2f, 1f, 1f);
        cellZero.transform.DOScale(scale, 1f);
        cellOne.transform.DOScale(scale, 1f).OnComplete(StartCellDivision);
    }

    void StartCellDivision()
    {
        separating = true;
        cellZero.transform.DOMove(cellZero.transform.position - Vector3.right * distanceBetweenTwo, 1f);
        cellOne.transform.DOMove(cellOne.transform.position + Vector3.right * distanceBetweenTwo, 1f).OnComplete(DisableLineRenderer);

        cellZero.transform.DOScale(Vector3.one, 1f).SetEase(Ease.OutBounce);
        cellOne.transform.DOScale(Vector3.one, 1f).SetEase(Ease.OutBounce);
    }
    void DisableLineRenderer()
    {
        StartCoroutine(DisableRoutine());
    }
    IEnumerator DisableRoutine()
    {
        float mult = ln.widthMultiplier;
        while (mult > 0)
        {
            mult -= Time.deltaTime;
            ln.widthMultiplier = mult;
            yield return ENDOFFRAME;
        }
        yield return ENDOFFRAME;
        mult = 0f;
        ln.widthMultiplier = mult;
        separating = false;
        ln.enabled = false;
    }
    void GetMiddlePoints()
    {
        middlePoints = new List<Vector3>();
        Vector3 mid = (cellZeroCore.position + cellOneCore.position) / 2f;

        for (int i = 0; i < connectionCount; i++)
        {
            float yy = mid.y;
            yy = (yy + (i * midPointDistance) ) - ((connectionCount * midPointDistance) / 2f) + (connectionCount/midPointDistance)/2f ;
            middlePoints.Add(new Vector3(mid.x, yy, mid.z));
        }
    }
    void SetLineRenderer()
    {        
        int totalPoints = (connectionCount * 2) + 1;
        Vector3[] points = new Vector3[totalPoints];
        ln.positionCount = totalPoints;
        bool isLeft = false;
        int midCount = 0;

        for (int i = 0; i < totalPoints - 1; i++) 
        {
            if (i % 2 == 0) 
            {
                if (isLeft)
                {
                    points[i] = cellOne.transform.position;
                }
                else
                {
                    points[i] = cellZero.transform.position;
                }
                isLeft = !isLeft;
            }
            else
            {
                points[i] = middlePoints[midCount];
                midCount++;
                
            }

        }
        points[totalPoints - 1] = cellZero.transform.position;
        ln.SetPositions(points);
    }
}
