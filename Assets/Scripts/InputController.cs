using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    [SerializeField] bool inputEnbled = true;
    [SerializeField] Transform cameraPivot;
    [SerializeField] float inputMult;

    readonly string FLOATER = "floater";

    [Header("Debug : ")]
    [SerializeField] Transform slingShot;
    Ray ray;
    RaycastHit hit;
    
    Vector3 mouseStart;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!inputEnbled)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            mouseStart = Input.mousePosition;
        }
        if (Input.GetMouseButton(0))
        {
            float rot = Input.GetAxis("Mouse X") * inputMult;
            if (Mathf.Abs( rot) < 0.2f)
                rot = 0;
            Debug.Log("rot: " + rot);
            rot += cameraPivot.localEulerAngles.y;
            
            //pivotCamera.RotateCamera(rot);//
            float rr = Mathf.Clamp(rot, 140, 220);
            //Debug.Log("horizontal: " + rr);
            cameraPivot.localEulerAngles = new Vector3(0f, rr, 0f);
        }
        if (Input.GetMouseButtonUp(0))
        {
            
        }
    }

    public void DiableInput() { inputEnbled = false; }
    public void EnableInput() { inputEnbled = true; }
}
