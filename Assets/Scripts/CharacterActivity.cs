using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterActivity : MonoBehaviour
{

    [SerializeField] Animator animator;
    [SerializeField] GameObject wheelChair;
    [SerializeField] GameObject bed;
    [SerializeField] GameObject bag;
    [SerializeField] GameObject stick;
    [SerializeField] GameObject mask;

    readonly string FULLHEALTH = "fullHealth";
    readonly string HALFHEALTH = "halfHealth";
    readonly string SICK = "sick";
    readonly string VERYSICK = "verySick";
    readonly string EXTREMESICK = "extremeSick";
    readonly string HOSPITAL = "hospital";

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FullHealth() { animator.SetTrigger(FULLHEALTH); }
    void HalfHealth() { animator.SetTrigger(HALFHEALTH); }
    void VerySick() {
        animator.SetTrigger(VERYSICK);
        DisableAllPrpos();
        stick.SetActive(true);
    }
    void ExtremeSick() {
        animator.SetTrigger(EXTREMESICK);
        DisableAllPrpos();
        wheelChair.SetActive(true);
        mask.SetActive(true);
    }
    void Hospital() {
        animator.SetTrigger(HOSPITAL);
        DisableAllPrpos();
        bed.SetActive(true);
        mask.SetActive(true);
    }

    void DisableAllPrpos()
    {
        wheelChair.SetActive(false);
        bed.SetActive(false);
        bag.SetActive(false);
        stick.SetActive(false);
        mask.SetActive(false);
    }

    public void LoadAsLevel(int stage)
    {
        switch (stage)
        {
            case 1:
                HalfHealth();
                break;
            case 2:
                VerySick();
                break;
            case 3:
                ExtremeSick();
                break;
            case 4:
                Hospital();
                break;
            default:
                break;
        }
    }
}
