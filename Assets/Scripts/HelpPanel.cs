using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpPanel : MonoBehaviour
{
    [SerializeField] Button helpButton;
    [SerializeField] Button closeButton;
    [SerializeField] GameObject textPanel;

    [SerializeField] Button linkButton0;
    [SerializeField] Button linkButton1;
    [SerializeField] Button linkButton2;

    [SerializeField] List<string> links;


    void Start()
    {
        HelpPanelOff();
        helpButton.onClick.AddListener(delegate
        {
            HelpPanelOn();
        });
        closeButton.onClick.AddListener(delegate
        {
            HelpPanelOff();
        });

        linkButton0.onClick.AddListener(delegate
        {
            Application.OpenURL(links[0]);
        });
        linkButton1.onClick.AddListener(delegate
        {
            Application.OpenURL(links[1]);
        });
        linkButton2.onClick.AddListener(delegate
        {
            Application.OpenURL(links[2]);
        });
    }

    void HelpPanelOn()
    {
        helpButton.interactable = false;
        closeButton.gameObject.SetActive(true);
        textPanel.gameObject.SetActive(true);
    }
    void HelpPanelOff()
    {
        helpButton.interactable = true;
        closeButton.gameObject.SetActive(false);
        textPanel.gameObject.SetActive(false);
    }
}
