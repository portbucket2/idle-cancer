using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Histogram : MonoBehaviour
{
    [SerializeField] bool isEnabled = false;
    [SerializeField] Transform startPos;
    [SerializeField] Transform endPos;

    [SerializeField] Transform pointer;
    [SerializeField] float speed;
    [SerializeField] float height;
    [SerializeField] float upBeatTime;
    [SerializeField] float downBeatTime;

    float counter = 0f;
    Vector3 pos;
    bool upDone = false;
    bool downDone = false;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isEnabled)
        {
            pointer.position = Vector3.Lerp(pointer.position, endPos.position, Time.deltaTime * speed);

            counter += Time.deltaTime;
            if (counter > upBeatTime && !upDone)
            {
                pos = new Vector3(pointer.position.x, pointer.position.y + height, pointer.position.z);
                upDone = true;
            }
            else
            {
                if (counter > downBeatTime && !downDone)
                {
                    pos = new Vector3(pointer.position.x, pointer.position.y - height / 2f, pointer.position.z);
                    downDone = true;
                }
                else
                {
                    pos = new Vector3(pointer.position.x, startPos.position.y, pointer.position.z);
                }

                //pos = new Vector3(pointer.position.x, startPos.position.y, pointer.position.z);
            }            

            pointer.position =  pos;

            if (Vector3.Distance(pointer.position, endPos.position) < 0.2f)
            {
                ResetPosition();
            }
        }
    }

    void ResetPosition()
    {
        pointer.position = startPos.position;
        counter = 0f;
        upDone = false;
        downDone = false;
    }
}
