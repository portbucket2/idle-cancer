using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BubblingCells : MonoBehaviour
{
    [SerializeField] GameObject cellPrefab;
    [SerializeField] float startRadius;
    [SerializeField] float radiusIncrement;
    [SerializeField] float cellGap = 0.7f;
    [SerializeField] CameraController camera;

    [Header("Debug: ")]
    [SerializeField] Transform lastCell;
    [SerializeField] Vector3 small;
    [SerializeField] Vector3 startScale;
    [SerializeField] float radius;
    [SerializeField] int cellCount;
    [SerializeField] float turnFraction = 1.61f;
    [SerializeField] int numPoints = 200;
    [SerializeField] float pow = 0.5f;
    [SerializeField] bool change = false;
    [SerializeField] List<Transform> cells;
    [SerializeField] List<Vector3> boids;


    readonly string CELLCOUNT = "CellCount";

    void Start()
    {
        radius = startRadius;
        cellCount = 0;
        small = new Vector3(0.2f, 0.2f, 0.2f);
        startScale = cellPrefab.transform.localScale;
        cells = new List<Transform>();
        //ProduceCell();
        boids = new List<Vector3>();
        CreateBoids();
        LoadCellCount();
    }

    public void ProduceCell()
    {
        if (boids.Count <= 0)
            return;


        GameObject newCell = Instantiate(cellPrefab, this.transform);
        cells.Add(newCell.transform);

        //if (cellCount==0)
        //{
        //    newCell.transform.position = transform.position;
        //}
        //else
        //{
        //    newCell.transform.position = transform.position + boids[cellCount];            
        //}
        newCell.transform.position = transform.position + boids[cellCount];
        cellCount++;


        newCell.transform.localScale = small;
        newCell.transform.DOScale(startScale, 1f).SetEase(Ease.OutBounce);
        lastCell = newCell.transform;

        PlayerPrefs.SetInt(CELLCOUNT, cellCount);

        RadiusCheck();

    }
    public void ProduceCellOld()
    {
        cellCount++;
        GameObject newCell = Instantiate(cellPrefab, this.transform);
        cells.Add(newCell.transform);

        if (lastCell)
        {
            Vector3 rand = transform.position + Random.onUnitSphere * radius;
            Vector3 newPos = new Vector3(rand.x, rand.y, transform.position.z);
            newCell.transform.position = newPos;
        }
        else
        {
            newCell.transform.position = transform.position + Random.onUnitSphere * radius;
        }

        newCell.transform.localScale = small;
        newCell.transform.DOScale(startScale, 1f).SetEase(Ease.OutBounce);
        lastCell = newCell.transform;

        RadiusCheck();

    }
    public void LoadCellCount()
    {
        int max = PlayerPrefs.GetInt(CELLCOUNT, 0);
        for (int i = 0; i < max; i++)
        {            
            ProduceCell();
        }
    }

    void RadiusCheck()
    {
        if (cellCount % 10 == 0) 
        {
            radius += radiusIncrement;
            camera.ZoomCheck(cells);
        }
        
    }

    void CreateBoids()
    {
        //float turnFraction = 1.61f;
        //int numPoints = 200;
        for (int i = 0; i < numPoints; i++)
        {
            float dst = i;// Mathf.Pow(i / (numPoints - 1f), pow);
            float angle = 2 * Mathf.PI * turnFraction * i;

            float x = dst * Mathf.Cos(angle);
            float y = dst * Mathf.Sin(angle);
            PlotPoint(x * cellGap, y * cellGap);
        }
    }

    void PlotPoint(float _x, float _y)
    {
        Vector3 pos = new Vector3(_x, _y, transform.position.z);
        boids.Add(pos);
    }
    //private void OnDrawGizmos()
    //{
    //    int max = boids.Count;
    //    for (int i = 0; i < max; i++)
    //    {
    //        Gizmos.color = Color.cyan;
    //        Gizmos.DrawSphere(transform.position + boids[i], 0.5f);
    //    }
       
    //}
}
