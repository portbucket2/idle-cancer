using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] GameObject levelCompletePanel;
    [SerializeField] Button NextLevelButton;

    GameManager gameManager;

    void Start()
    {
        gameManager = GameManager.GetManager();
        NextLevelButton.onClick.AddListener(delegate
        {
            gameManager.GotoNextStage();
        });
    }

    public void LevelComplete()
    {
        levelCompletePanel.SetActive(true);
    }
}
