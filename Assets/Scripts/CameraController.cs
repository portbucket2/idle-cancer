using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraController : MonoBehaviour
{
    [SerializeField] float maxZoom;
    [SerializeField] float minZoom;
    [SerializeField] float zoomLimit;
    [SerializeField] float transitionDuration;
    [SerializeField] float introFov;
    [SerializeField] float introNormal;
    [SerializeField] float exitFov;
    [SerializeField] float stageTransition;

    Camera cam;
    bool isZooming = false;
    float boundSize;

    GameController gameController;

    void Start()
    {
        gameController = GameController.GetController();
        cam = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ZoomCheck(List<Transform> targets)
    {
        var bounds = new Bounds(targets[0].position, Vector3.zero);
        int max = targets.Count;
        for (int i = 0; i < max; i++)
        {
            bounds.Encapsulate(targets[i].position);
        }
        boundSize = bounds.size.x;
        Zoom();

    }
    void Zoom()
    {
        float newZoom = Mathf.Lerp(maxZoom, minZoom, boundSize / zoomLimit);

        //cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, newZoom, Time.deltaTime);

        cam.DOFieldOfView(newZoom, transitionDuration);
    }

    public void Intro()
    {
        NextStage();
        cam.fieldOfView = introFov;
        cam.DOFieldOfView(introNormal, stageTransition);
    }
    public void Exit()
    {
        cam.DOFieldOfView(exitFov, stageTransition).SetEase(Ease.OutSine).OnComplete(Exited);
    }
    void Exited()
    {
        gameController.StartOrganCamera();
    }
    void NextStage()
    {
        gameController.GetSceneController().CancerColonStage();
    }
}
