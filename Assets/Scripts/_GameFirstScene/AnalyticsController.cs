﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using com.faithstudio.SDK;
using GameAnalyticsSDK;
using LionStudios.Suite.Analytics;
using LionStudios.Suite.Debugging;


public class AnalyticsController : MonoBehaviour
{
    public static AnalyticsController analyticsController;

    int levelcount = 1;
    int totalCoin = 0;

    GameManager gameManager;
    private void Awake()
    {
        levelcount = 1;
        analyticsController = this;
        gameManager = transform.GetComponent<GameManager>();
    }
    private void Start()
    {
        levelcount = gameManager.GetDataManager().GetGamePlayer.levelsCompleted;
        GameAnalytics.Initialize();
        Application.targetFrameRate = 60;
        LionAnalytics.GameStart();
        LionDebugger.Hide();
    }

    public static AnalyticsController GetController()
    {
        return analyticsController;
    }

    public void LevelStarted()
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "World01", "Stage " + levelcount);
        LionAnalytics.LevelStart(levelcount, 1, totalCoin);
        SaveGame();
    }
    public void LevelCompleted()
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "World01", "Stage " + levelcount);
        LionAnalytics.LevelComplete(levelcount, 1, totalCoin);
        levelcount++;
        SaveGame();
    }
    public void LevelFailed()
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "World01", "Stage " + levelcount);
        LionAnalytics.LevelFail(levelcount, 1, totalCoin);
    }
    public void SaveGame()
    {
        GamePlayer gp = new GamePlayer();
        gp.name = "";
        gp.id = 1;
        gp.levelsCompleted = levelcount;
        gp.totalCoins = totalCoin;
        gp.lastPlayedLevel = CancerController.Instance.currentCancerStage;
        gp.handTutorialShown = gameManager.HasTutorialSeen();

        gameManager.GetDataManager().SetGameplayerData(gp);
    }
    public void UpdateCoin(int _coin)
    {
        totalCoin = _coin;
    }
    public void EventButtonPress(string buttonName, float currentValue)
    {
        GameAnalytics.NewDesignEvent(buttonName, levelcount);
    }

    
    public void GameEnd() {
        GameAnalytics.NewDesignEvent("Game End", 0);
    }
    public void OfflineIncomeCollect(float amount) {
        GameAnalytics.NewDesignEvent("Offline Income collect", amount);
    }
}
