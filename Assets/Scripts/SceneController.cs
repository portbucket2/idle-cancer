using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SceneController : MonoBehaviour
{
    [Header("Holders")]
    [SerializeField] GameObject cellHolder;
    [SerializeField] GameObject bubblingCellHolder;
    [SerializeField] GameObject organHolder;
    [SerializeField] Animator transitionSphere;
    [Header("BGs")]
    [SerializeField] GameObject cellsBG;
    [SerializeField] GameObject gradientBG;
    [Header("Particles")]
    [SerializeField] ParticleSystem tapIncomeParticle;
    [SerializeField] ParticleSystem skullParticle;


    [Header("Debug")]    
    [SerializeField] CellSplitter cellSplitter;
    [SerializeField] BubblingCells bubblingCells;
    [SerializeField] OrganInfection organInfection;

    [Header("Cameras")]
    [SerializeField] CameraController cellCamera;
    [SerializeField] CameraController organCamera;

    WaitForSeconds WAIT = new WaitForSeconds(0.1f);

    GameController gameController;
    AnalyticsController analytics;
    private void Awake()
    {
        //cellSplitter = cellHolder.GetComponent<CellSplitter>();
        //bubblingCells = bubblingCellHolder.GetComponent<BubblingCells>();
        //organInfection = organHolder.GetComponent<OrganInfection>();
    }
    void Start()
    {
        gameController = GameController.GetController();
        analytics = AnalyticsController.GetController();

        cellCamera = gameController.GetCellCamera();
        organCamera = gameController.GetOrganCamera();

        cellsBG.SetActive(true);
        gradientBG.SetActive(false);

        CancerCellStage();

        StartCoroutine(StartDelay());
    }

    IEnumerator StartDelay()
    {
        yield return WAIT;
        int currentStage = CancerController.Instance.currentCancerStage;
        LoadVisualStage(currentStage);
        gameController.GetCharacter().LoadAsLevel(currentStage);
    }

    public void CancerCellStage()
    {
        cellHolder.SetActive(true);
        bubblingCellHolder.SetActive(true);
        organHolder.SetActive(false);

        cellCamera.gameObject.SetActive(true);
        organCamera.gameObject.SetActive(false);
    }

    public void CancerBubbleStage()
    {
        Debug.Log("bubble satge!!");
        cellHolder.SetActive(true);
        bubblingCellHolder.SetActive(true);
        organHolder.SetActive(false);
        gameController.GetTutorial().FirstStageComplete();
    }
    
    public void CancerColonStage()
    {
        gameController.GetInputController().EnableInput();
        cellsBG.SetActive(false);
        gradientBG.SetActive(true);

        cellHolder.SetActive(false);
        bubblingCellHolder.SetActive(false);
        organHolder.SetActive(true);

        cellCamera.gameObject.SetActive(false);
        organCamera.gameObject.SetActive(true);
    }
   
    public void CancerLiverStage()
    {
        gameController.GetInputController().EnableInput();
        cellsBG.SetActive(false);
        gradientBG.SetActive(true);

        cellHolder.SetActive(false);
        bubblingCellHolder.SetActive(false);
        organHolder.SetActive(true);


        cellCamera.gameObject.SetActive(false);
        organCamera.gameObject.SetActive(true);
    }


    // call for effect
    public void SplitCell()
    {
        cellSplitter.SplitTheCell();
    }
    public void Bubble()
    {
        bubblingCells.ProduceCellOld();
    }
    public void InfectColon()
    {
        organInfection.InfectColon();
    }
    public void CancerTransmissionStage()
    {
        transitionSphere.SetTrigger("move");
    }
    public void InfectLiver()
    {
        organInfection.InfectLiver();
    }
    public void CancerCameraChange()
    {
        cellCamera.Exit();
    }
    public void LoadVisualStage(int stage)
    {
        analytics.LevelStarted();
        if (stage == 1)
        {
            CancerCellStage();
        }
        else if (stage == 2)
        {
            CancerBubbleStage();
        }
        else if (stage == 3)
        {
            CancerColonStage();
        }
        else if (stage == 4)
        {
            CancerLiverStage();
        }
    }
    public void TapIncomeParticle()
    {
        tapIncomeParticle.Play();
    }
    public void SkullParticle()
    {
        skullParticle.Play();
    }
}
