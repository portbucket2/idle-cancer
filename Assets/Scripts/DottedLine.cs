using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DottedLine : MonoBehaviour
{
    Ray ray;
    Ray ray2;
    RaycastHit hit;
    RaycastHit hit2;

    LineRenderer ln;

    readonly string WALL = "wall";
    LayerMask Ignor;
    
    void Start()
    {
        ln = GetComponent<LineRenderer>();
        ln.enabled = false;
        Ignor = LayerMask.GetMask("Prop");
    }
    public void DisableLine() { ln.enabled = false; }
    public void ShowLine(Vector3 _dir, Vector3 _pos)
    {
        ln.enabled = true;
        ray = new Ray(_pos, _dir);

        if (Physics.Raycast(ray, out hit, 100f,~Ignor))
        {
            if (hit.transform.CompareTag(WALL))
            {
                // recoche
                ray2 = new Ray(hit.point, Vector3.Reflect(_dir, hit.normal));
                if (Physics.Raycast(ray2, out hit2, 100f))
                {
                    ln.positionCount = 3;
                    ln.SetPosition(0, _pos);
                    ln.SetPosition(1, hit.point);
                    ln.SetPosition(2, hit2.point);
                }
            }
            else
            {
                ln.positionCount = 2;
                ln.SetPosition(0, _pos);
                ln.SetPosition(1, hit.point);
                //Debug.DrawLine(_pos, hit.point, Color.blue);
            }
        }
        else
        {
            ln.positionCount = 2;
            ln.SetPosition(0, _pos);
            ln.SetPosition(1, _pos + _dir * 3f);
            Debug.DrawLine(_pos, hit.point, Color.blue);
        }
    }
}
